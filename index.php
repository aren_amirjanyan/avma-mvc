<?php

use core\AvmaMVC;

$configs = [
    'app'=>require_once 'config/app.php',
    'db' => require_once 'config/db.php',
    'params' => require_once 'config/params.php'
];

require_once 'core/autoload.php';

$app = new AvmaMVC($configs);
$app->run();


