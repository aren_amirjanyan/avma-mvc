<?php
/**
 * Created by PhpStorm.
 * User: Aren
 * Date: 9/3/2017
 * Time: 10:33 PM
 */

use core\Controller;
use core\Request;
use models\Users;
use models\Tasks;
use models\UsersTasks;
use models\TasksImages;
use core\Upload;

class TaskController extends Controller
{
    private $request;
    public $auth = false;
    private $files = [];
    public $author;

    public function __construct($controller, $action, $parameters, $configs)
    {
        parent::__construct($controller, $action, $parameters, $configs);
        $this->request = new Request();
        session_start();
        if(!empty($_SESSION['user'])){
            $this->auth = true;
            $this->author = $_SESSION['user']['first_name'].' '.$_SESSION['user']['last_name'];
        }else{
            $this->auth = false;
            $this->author = null;
        }


    }

    public function index()
    {
        $parameters = $this->parameters;
        $page = (!empty($parameters) && !empty($parameters['page'])) ? $parameters['page']:1;
        $length = 6;

        $tasks = new Tasks();
        $allTasks = $tasks->select_table('SELECT t.id, t.name, t.description, t.author,t.status,t.created_at, u.username as assigned, ti.image as task_image FROM tasks as t LEFT JOIN users_tasks as ut ON t.id = ut.task_id LEFT JOIN users as u ON ut.user_id=u.id LEFT JOIN tasks_images as ti ON ti.task_id = t.id');
        $filterAllTasks = [];
        if(!empty($allTasks)){

            foreach ($allTasks as $key => $value){
                $filterAllTasks['task_'.$value['id']]['id'] = $value['id'];
                $filterAllTasks['task_'.$value['id']]['name'] = $value['name'];
                $filterAllTasks['task_'.$value['id']]['description'] = $value['description'];
                $filterAllTasks['task_'.$value['id']]['author'] = $value['author'];
                $filterAllTasks['task_'.$value['id']]['status'] = $value['status'];
                $filterAllTasks['task_'.$value['id']]['images'][] = $value['task_image'];

                if(!isset($filterAllTasks['task_'.$value['id']]['assigned'])){
                    $filterAllTasks['task_'.$value['id']]['assigned'] = [];
                }

                if(!in_array($value['assigned'],$filterAllTasks['task_'.$value['id']]['assigned'])){
                    $filterAllTasks['task_'.$value['id']]['assigned'][] = $value['assigned'];
                }
                $filterAllTasks['task_'.$value['id']]['created_at'] = $value['created_at'];

            }
        }
        $data['tasks'] = array_slice($filterAllTasks,($page-1)*$length,$length);
        $data['count'] = count($filterAllTasks);

        $data['pages'] = (int)$data['count']/$length;
        $data['page'] = $page;

        $this->render('index',$data);
    }
    public function create(){
        if(!empty($this->request->post())){
            $this->files = (!empty($_FILES) && !empty($_FILES['task_images']))? $_FILES['task_images']:[];
            $userName = $this->request->post('user_name');
            $userEmail = $this->request->post('user_email');
            $user = new Users();
            $userData = $user->select('id',"username='$userName' and email='$userEmail'");
            if(!empty($userData)){
                $userId = $userData[0]['id'];
            }else{
                $password = md5($userName);
                $created_at = date('Y-m-d h:s:i',time());
                $newUserId = $user->insert(['first_name','last_name','username','email','password','status','created_at'],["'$userName'","'$userName'","'$userName'","'$userEmail'","'$password'",1,"'$created_at'"]);
                if($newUserId){
                    $userId = $newUserId;
                }
            }
            $taskName = $this->request->post('task_name');
            $taskDescription = $this->request->post('task_description');
            $status = 'new';
            $author = ($this->auth)?$_SESSION['user']['first_name'].' '.$_SESSION['user']['last_name'] : null;
            $created_at = date('Y-m-d h:s:i',time());
            $task = new Tasks();
            $newTaskId = $task->insert(['name','description','status','author','created_at'],["'$taskName'","'$taskDescription'","'$status'","'$author'","'$created_at'"]);

            if($newTaskId){

                $taskId = $newTaskId;

                if(!is_dir ("uploads/tasks/$taskId"))
                {
                    if (!mkdir("uploads\\tasks\\$taskId\\", 7777, true)) {
                        die('Failed to create folders...');
                    }
                }
                $usersTasks = new UsersTasks();
                $created_at = date('Y-m-d h:s:i',time());
                $updated_at = date('Y-m-d h:s:i',time());
                /*Assigned task to user*/

                $newUsersTasks = $usersTasks->insert(['user_id','task_id','created_at','updated_at'],["'$userId'","'$taskId'","'$created_at'","'$updated_at'"]);

                if($newUsersTasks && !empty($this->files)){
                    $tasksImages = new TasksImages();
                    if(!empty($this->files['name'])){
                        foreach ($this->files['name'] as $key => $value){
                            $name = $value;
                            $tmpImageFile = $this->files['tmp_name'][$key];
                            $size = $this->files['size'][$key];
                            $created_at = date('Y-m-d h:s:i',time());
                            $newTasksImages = $tasksImages->insert(['task_id','image','created_at'],["'$taskId'","'$name'","'$created_at'"]);
                            if($newTasksImages){
                                $upload = new Upload($name,$tmpImageFile,$size,"uploads/tasks/$taskId/");
                                if($upload->save()){
                                    $this->redirect('/task/index');
                                }
                            }
                        }
                    }
                }


            }

            print_r($this->request->post());die;
        }
    }
}