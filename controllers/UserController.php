<?php
/**
 * Created by PhpStorm.
 * User: Aren
 * Date: 9/3/2017
 * Time: 10:33 PM
 */

use core\Controller;
use core\Request;

class UserController extends Controller
{
    private $request;
    public $auth = false;

    public function __construct($controller, $action, $parameters, $configs)
    {
        parent::__construct($controller, $action, $parameters, $configs);
        $this->request = new Request();
        session_start();
        if(!empty($_SESSION['user'])){
            $this->auth = true;
        }else{
            $this->auth = false;
            session_destroy();
            $this->redirect('/site/login');
        }


    }

    public function index()
    {
        $data['user'] = $_SESSION['user'];
        $this->render('index', $data);
    }
}