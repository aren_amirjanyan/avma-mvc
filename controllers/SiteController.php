<?php
/**
 * Created by PhpStorm.
 * User: Aren
 * Date: 9/3/2017
 * Time: 10:33 PM
 */

use core\Controller;
use core\Request;
use models\Users;

class SiteController extends Controller
{
    private $request;
    public $auth = false;

    public function __construct($controller, $action, $parameters, $configs)
    {
        parent::__construct($controller, $action, $parameters, $configs);
        $this->request = new Request();
        session_start();
        if(!empty($_SESSION['user'])){
            $this->auth = true;
        }else{
            $this->auth = false;
        }

    }

    public function index()
    {
        $data['parameters'] = $this->parameters;
        $this->render('index', $data);
    }

    public function about()
    {
        $this->render('about');
    }

    public function contact()
    {
        $this->render('contact');
    }

    public function login()
    {
        if (!empty($this->request->post()) && $this->request->post('username') && $this->request->post('password')) {
            $username = $this->request->post('username');
            $password = sha1($this->request->post('password')); // for admin password is Admin2023!!!
            $user = new Users();
            $userData = $user->select('*', "username='$username' and password='$password'");
            if(!empty($userData[0])){
                session_start();
                $_SESSION['user'] = $userData[0];
                $this->redirect('/user/index');
            }
            else
            $this->redirect('/site/login');
        }
        $this->render('login');
    }

    public function register()
    {
        $this->render('register');
    }
    public function logout()
    {
        session_start();
        session_destroy();
        $this->redirect('/site/login');
    }

}