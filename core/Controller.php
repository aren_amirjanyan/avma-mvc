<?php
/**
 * Created by PhpStorm.
 * User: Aren
 * Date: 9/3/2017
 * Time: 9:05 PM
 */

namespace core;


class Controller
{
    public $controller;
    public $action;
    public $parameters;
    public $configs;
    public $content;

    public $layout = 'site';

    public function __construct($controller, $action, $parameters, $configs)
    {
        $this->controller = $controller;
        $this->action = $action;
        $this->parameters = $parameters;
        $this->configs = $configs;
        $this->content = 'views/404.php';
    }

    public function render($view, $data = [])
    {

        if (!empty($data)) {
            foreach ($data as $key => $value) {
                $$key = $value;
            }
        }
        if (file_exists('views/' . $this->controller . '/' . $view . '.php'))
            $this->content = 'views/' . $this->controller . '/' . $view . '.php';

        require_once 'views/layouts/' . $this->layout . '.php';
    }

    public function redirect($page)
    {
        header('Location: ' . $this->configs['app']['site_url'] . $page);
    }

}