<?php
/**
 * Created by PhpStorm.
 * User: Aren
 * Date: 9/4/2017
 * Time: 4:36 PM
 */

namespace core;


class Upload
{
    public $name;
    public $tmp_name;
    public $size;
    public $targed_dir;
    public $target_file;
    public $target_dir;
    public $imageFileType;
    public $defaultSize = 500000;
    public $defaultImageTypes = ['jpg', 'png', 'jpeg', 'gif'];
    private $uploadOk = true;

    public function __construct($name, $tmp_name, $size,$targedDir = '/uploads')
    {
        $this->name = $name;
        $this->tmp_name = $tmp_name;
        $this->size = $size;
        $this->target_dir = $targedDir;
        $this->target_file = $this->target_dir . basename($this->name);

        $this->imageFileType = pathinfo($this->target_file, PATHINFO_EXTENSION);

    }
    public function save(){

        // Check if image file is a actual image or fake image
        if (!empty($this->tmp_name)) {
            $check = getimagesize($this->tmp_name);
            if ($check !== false) {
                echo "File is an image - " . $check["mime"] . ".";
                $this->uploadOk = true;
            } else {
                echo "File is not an image.";
                $this->uploadOk = false;
            }
        }
        // Check if file already exists
        if (file_exists($this->target_file)) {
            echo "Sorry, file already exists.";
            $this->uploadOk = true;
        }
        // Check file size
        if ($this->size > $this->defaultSize) {
            echo "Sorry, your file is too large.";
            $this->uploadOk = false;
        }
        // Allow certain file formats
        if (!in_array($this->imageFileType, $this->defaultImageTypes)) {
            $only = implode(',', $this->defaultImageTypes);
            echo "Sorry, $only files are allowed.";
            $this->uploadOk = false;
        }
        // Check if $uploadOk is set to 0 by an error
        if ($this->uploadOk == false) {
            echo "Sorry, your file was not uploaded.";
            // if everything is ok, try to upload file
        } else {
            if (move_uploaded_file($this->tmp_name, $this->target_file)) {
                echo "The file " . basename($this->name) . " has been uploaded.";
            } else {
                echo "Sorry, there was an error uploading your file.";
            }
        }

        return $this->uploadOk;
    }

}