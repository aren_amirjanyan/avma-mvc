<?php
//Define autoloader

class Autoload {

    private $classes;

    public function __construct()
    {
        $this->classes = array_slice(scandir(__DIR__), 2);
        $this->models = array_slice(scandir('models'), 2);
    }

    public function init(){
        if(!empty($this->classes)){
            foreach ($this->classes as $class){
                    $this->__autoloadCore($class);
            }
        }
        if(!empty($this->models)){
            foreach ($this->models as $model){
                $this->__autoloadModels($model);
            }
        }
    }

    private function __autoloadCore($className) {

        if (file_exists(__DIR__.'/'.$className) && $className !== 'autoload.php') {

            require_once __DIR__.'/'.$className;

            return true;
        }
        return false;
    }
    private function __autoloadModels($className) {

        if (file_exists('models/'.$className)) {

            require_once 'models/'.$className;

            return true;
        }
        return false;
    }
}
$autoload = new Autoload();
$autoload->init();
?>