<?php
/**
 * Created by PhpStorm.
 * User: Aren
 * Date: 9/3/2017
 * Time: 9:06 PM
 */

namespace core;

use PDO;
use PDOException;


class Database
{
    private $configs;
    private $servername;
    private $username;
    private $password;
    private $database;
    private $port;
    private $driver;

    public $connect;

    public function __construct($configs)
    {
        $this->configs = $configs;
        $this->servername = $this->configs['DB_HOST'];
        $this->username = $this->configs['DB_USERNAME'];
        $this->password = $this->configs['DB_PASSWORD'];
        $this->database = $this->configs['DB_DATABASE'];
        $this->port = $this->configs['DB_PORT'];
        $this->driver = $this->configs['DB_CONNECTION'];

        $this->connect = new PDO("$this->driver:host=$this->servername;dbname=$this->database", $this->username, $this->password);
        // set the PDO error mode to exception
        $this->connect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    public function createDB($dbName)
    {
        try {
            $query = "CREATE DATABASE $dbName";
            // use exec() because no results are returned
            $this->connect->exec($query);
            echo "Database created successfully";
        } catch (PDOException $e) {
            echo $query . "<br>" . $e->getMessage();
        }
        $this->connect = null;
    }

    public function createTable($table, $fields)
    {
        $queryFields = implode(',', $fields);
        try {
            $query = "CREATE TABLE $table ($queryFields)";
            $this->connect->exec($query);
            echo "Table $table created successfully";
        } catch (PDOException $e) {
            echo $query . "<br>" . $e->getMessage();
        }
        $this->connect = null;
    }

    public function insert_table($table, $fields, $values)
    {
        $queryFields = implode(',', $fields);
        $queryValues = implode(',', $values);
        try {
            $query = "INSERT INTO $table ($queryFields) VALUES ($queryValues)";
            $this->connect->exec($query);
            $last_id = $this->connect->lastInsertId();
            return $last_id;
        } catch (PDOException $e) {
            echo $query . "<br>" . $e->getMessage();
        }
        $this->connect = null;
    }

    public function select_table($query)
    {
        try {
            $stmt = $this->connect->prepare($query);
            $stmt->execute();
            // set the resulting array to associative
            $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
            return $stmt->fetchAll();
        } catch (PDOException $e) {
            echo "Error: " . $e->getMessage();
        }
        $this->connect = null;
    }

    public function update_table($query)
    {
        try {
            $stmt = $this->connect->prepare($query);
            $stmt->execute();
            // echo a message to say the UPDATE succeeded
            return $stmt->rowCount() . " records UPDATED successfully";
        } catch (PDOException $e) {
            echo $query . "<br>" . $e->getMessage();
        }
        $this->connect = null;
    }

    public function delete_table($table, $row)
    {
        try {
            $query = "DELETE FROM $table WHERE id=$row";
            $this->connect->exec($query);
            // echo a message to say the UPDATE succeeded
            return "Record deleted successfully";
        } catch (PDOException $e) {
            echo $query . "<br>" . $e->getMessage();
        }
        $this->connect = null;
    }
}