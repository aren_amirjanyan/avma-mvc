<?php
/**
 * Created by PhpStorm.
 * User: Aren
 * Date: 9/3/2017
 * Time: 10:05 PM
 */

namespace core;

use PDOException;


class AvmaMVC
{
    public $configs;

    public $request;

    public $db;

    public $controller;
    public $action;
    public $parameters;

    private $defaultAction = 'index';

    private $defaultController;

    public function __construct($configs)
    {
        $this->configs = $configs;
        $this->request = new Request();

        try {
            $this->db = new Database($this->configs['db']);
        }
        catch(PDOException $e)
        {
            echo "Connection failed: " . $e->getMessage();
        }

        $this->defaultController = $configs['app']['defaultController'];


        $this->controller = $this->defaultController;
        $this->action = $this->defaultAction;
        $this->parameters =  [];
    }
    public function run(){

            if(!empty($this->request->get())){
                $this->controller = (!empty($this->request->get('controller'))) ? $this->request->get('controller') : $this->defaultController;
                $this->action = (!empty($this->request->get('action'))) ? $this->request->get('action') : $this->defaultAction;
                $var = $this->request->get();
                $this->parameters = (!empty($this->request->get('controller')) && !empty($this->request->get('action'))) ? array_splice($var,2) : [];

            }

            $controllerName = ucfirst($this->controller).'Controller';

        if(file_exists('controllers/'. $controllerName.'.php'))
        {
            require_once 'controllers/'. $controllerName.'.php';
            $controller = new $controllerName($this->controller,$this->action,$this->parameters,$this->configs);
            $action = $this->action;
            if(!method_exists($controller,$action)){
                $controller->content = 'views/issue.php';
                $controller->render('issue');
                die;
            }
            $controller->$action();
        }
        else
            require_once 'views/404.php';
   }

}