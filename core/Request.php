<?php
/**
 * Created by PhpStorm.
 * User: Aren
 * Date: 9/3/2017
 * Time: 9:06 PM
 */

namespace core;


class Request
{
    public $method;

    public function __construct()
    {
        switch ($_SERVER['REQUEST_METHOD']) {
                case 'GET':
                    $this->method = 'GET';
                    break;
                case 'POST':
                    $this->method = 'POST';
                    break;
                case 'DELETE':
                    $this->method = 'DELETE';
                    break;
                case 'PUT':
                    $this->method = 'PUT';
                    break;
                default:
                    $this->method = 'OPTIONS';
            }
    }
    public function post($name = null){
        if(!empty($name))
            return $_POST[$name];
        return $_POST;
    }
    public function get($name = null){
        if(!empty($name))
            return $_GET[$name];
        return $_GET;
    }
}