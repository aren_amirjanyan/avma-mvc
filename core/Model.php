<?php
/**
 * Created by PhpStorm.
 * User: Aren
 * Date: 9/3/2017
 * Time: 9:06 PM
 */

namespace core;

use core\Database;
use PDO;
use PDOException;


class Model extends Database
{
    public $db;
    public $table;
    public $configs;

    public function __construct()
    {
        $this->configs = require 'config/db.php';
        parent::__construct($this->configs);
    }

    public function select($fields,$conditions){
        $query = "SELECT " .$fields." FROM ".$this->table.' WHERE '.$conditions;
        return $this->select_table($query);
    }
    public function insert($fields,$values){
        return $this->insert_table($this->table,$fields,$values);
    }
    public function update($query){
        return $this->update_table($query);
    }
    public function delete($row){
        return $this->delete_table($this->table,$row);
    }
}