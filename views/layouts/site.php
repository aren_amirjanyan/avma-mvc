<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $this->configs['app']['title'];?></title>
    <link href="<?php echo $this->configs['app']['site_url']?>/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $this->configs['app']['site_url']?>/css/site.css" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link rel="icon" href="favicon.ico">
<head>
<body>
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="<?php echo $this->configs['app']['site_url']?>"><?php echo $this->configs['app']['title'];?></a>
        </div>
        <ul class="nav navbar-nav">
            <li class="<?php echo ($this->controller != 'task' && $this->action == 'index')?'active':''; ?> ">
                <a href="<?php echo $this->configs['app']['site_url']?>/site/index">Home</a>
            </li>
            <li class="<?php echo ($this->controller == 'task' && $this->action == 'index')?'active':''; ?>">
                <a href="<?php echo $this->configs['app']['site_url']?>/task/index">Tasks</span></a>
            </li>
            <li class="<?php echo ($this->action == 'about')?'active':''; ?>">
                <a href="<?php echo $this->configs['app']['site_url']?>/site/about">About Us</a>
            </li>
            <li class="<?php echo ($this->action == 'galery')?'active':''; ?>">
                <a href="<?php echo $this->configs['app']['site_url']?>/site/galery">Galery</a>
            </li>
            <li class=" <?php echo ($this->action == 'contact')?'active':''; ?>">
                <a href="<?php echo $this->configs['app']['site_url']?>/site/contact">Contact</a>
            </li>
        </ul>
        <ul class="nav navbar-nav">
            <li class="<?php echo ($this->action == 'login')?'active':''; ?>">
                <?php if(!$this->auth) {
                    ?><a  href="<?php echo $this->configs['app']['site_url']?>/site/login">Login</a><?php
                }else {
                    ?>
                    <a  href="<?php echo $this->configs['app']['site_url']?>/site/logout">Logout (<?php echo $_SESSION['user']['username']; ?>)</a>
                    <?php
                } ?>
            </li>
            <?php if(!$this->auth) { ?><li class=" <?php echo ($this->action == 'register')?'active':''; ?>"">
                <a  href="<?php echo $this->configs['app']['site_url']?>/site/register">Register</a>
                </li> <?php } ?>
        </ul>
    </div>
</nav>
<div class="container">
<?php require_once $this->content; ?>
</div>
<footer class="footer">
    <div class="container">
        <span class="text-muted"> Copyright 2017 by Refsnes Data. All Rights Reserved. Powered by Aren Amirjanyan</span>
    </div>
</footer>
<script src="<?php echo $this->configs['app']['site_url']?>/js/jquery.min.js"></script>
<script src="<?php echo $this->configs['app']['site_url']?>/js/bootstrap.min.js"></script>
<script src="<?php echo $this->configs['app']['site_url']?>/js/scripts.js"></script>
</body>
</html>