<div class="row">
    <div class="container">
        <h1>This is <?php echo $this->action;?> page</h1>
        <div class="modal-dialog">
            <div class="loginmodal-container">
                <h1>Login to Your Account</h1><br>
                <form method="post" action="<?php echo $this->configs['app']['site_url'];?>/site/login">
                    <input type="text" name="username" placeholder="Username">
                    <input type="password" name="password" placeholder="Password">
                    <input type="submit" name="login" class="login loginmodal-submit" value="Login">
                </form>

                <div class="login-help">
                    <a href="<?php echo $this->configs['app']['site_url'];?>/site/register">Register</a> - <a href="<?php echo $this->configs['app']['site_url'];?>/site/forgot">Forgot Password</a>
                </div>
            </div>
        </div>
    </div>
</div>
