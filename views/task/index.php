<div class="row">
    <!-- Modal -->
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add New Task</h4>
                </div>
                <div class="modal-body">
                    <form enctype="multipart/form-data" method="post"
                          action="<?php echo $this->configs['app']['site_url'] ?>/task/create">
                        <div class="form-group">
                            <label for="user-name">Task Name</label>
                            <input type="text" class="form-control" name="task_name" id="task-name" placeholder="Task Name"/>
                            <input type="hidden" class="form-control" name="task_author" id="task-author" value="<?php echo $this->author; ?>"/>
                        </div>
                        <div class="form-group">
                            <label for="task-description">Task Description</label>
                            <textarea class="form-control" name="task_description" id="task-description"
                                      placeholder="Task Description"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="task-images">Add Images</label>
                            <input type="file" class="form-control" name="task_images[]" id="task-images"
                                   placeholder="Task Images" multiple/>
                        </div>
                        <div class="form-group">
                            <label for="user_name">Assign To (username)</label>
                            <input type="text" class="form-control" name="user_name" id="user-name"
                                   placeholder="User Name"/>
                        </div>
                        <div class="form-group">
                            <label for="user_email">Assign To (email)</label>
                            <input type="text" class="form-control" name="user_email" id="user-email"
                                   placeholder="User Email"/>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-success" id="add-new-task">Save Task</button>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="preview">Preview
                    </button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
    <div class="modal fade" id="modalPreview" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content-preview">
                <div class="modal-body-preview">
                    <div class="col-md-4">
                        <div class="column">
                            <!-- Post-->
                            <div class="post-module">
                                <!-- Thumbnail-->
                                <div class="thumbnail">
                                    <img id="preview-task-image" src="" class="img-responsive" alt=""></div>
                                <!-- Post Content-->
                                <div class="post-content">
                                    <div class="category" id="preview-task-status"></div>
                                    <h1 class="title" id="preview-task-title"></h1>
                                    <h2 class="sub_title" id="preview-task-author"></h2>
                                    <p class="description" id="preview-task-description"></p>
                                    <div class="post-meta">
                                    <span class="timestamp"><i class="fa fa-clock-" id="preview-task-created"></i></span>
                                        <span class="comments" id="preview-task-assigned"></span>
                                    </div>
                                    <a href="<?php echo $this->configs['app']['site_url'] ?>/task/view" class="card-link">View Task</a>
                                </div>
                            </div>
                        </div>
                        <button type="button" class="btn btn-default" id="close-preview">Close</button>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="container" id="tourpackages-carousel">
        <div class="row">
            <div class="col-lg-12">
                <h1>Tasks
                    <button type="button" class="btn icon-btn btn-success pull-right" data-toggle="modal"
                            data-target="#myModal">
                        <span class="glyphicon btn-glyphicon glyphicon-plus img-circle"></span>Add New Task
                    </button>
                </h1>
            </div>
        </div><!-- End row -->
    </div>
    <div class="row">
        <?php if (!empty($tasks)) {
            foreach ($tasks as $key => $value) {
                $taskId = $value['id'];
                $image = (!empty($value['images'])) ? $value['images'][0] : null;
                $imageUrl = '';
                if ($image) {
                    $imageUrl = "/uploads/tasks/$taskId/$image";

                }
                ?>
                <div class="col-md-4">
                    <div class="column">

                        <!-- Post-->
                        <div class="post-module">
                            <!-- Thumbnail-->
                            <div class="thumbnail">
                                <img src="<?php echo $imageUrl; ?>" class="img-responsive"
                                     alt="<?php echo $value['name']; ?>"></div>
                            <!-- Post Content-->
                            <div class="post-content">
                                <div class="category"><?php echo $value['status']; ?></div>
                                <h1 class="title"><?php echo $value['name']; ?></h1>
                                <h2 class="sub_title"><?php echo $value['author']; ?></h2>
                                <p class="description"><?php echo $value['description']; ?></p>
                                <div class="post-meta">
                                    <span class="timestamp"><i
                                                class="fa fa-clock-"></i><?php echo $value['created_at']; ?></span>
                                    <span class="comments"><i class="fa fa-comments"></i><a
                                                href="#"> <?php echo implode(', ', $value['assigned']); ?></a></span>
                                </div>
                                <a href="<?php echo $this->configs['app']['site_url'] ?>/task/view/<?php echo $taskId; ?>"
                                   class="card-link">View Task</a>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
            }
        } ?>

    </div>
</div>
<div class="row">
    <nav aria-label="">
        <ul class="pagination">
            <li class="page-item disabled">
                <a class="page-link"
                   href="<?php echo $this->configs['app']['site_url'] ?>/task/index?page=<?php echo ($page - 1 >= 0) ? ($page - 1) : 1; ?>"
                   tabindex="-1">Previous</a>
            </li>
            <?php for ($i = 0; $i < $pages; $i++) {
                ?>
                <li class="page-item <?php echo ($i + 1 == $page || (!isset($page) && ($i + 1 == 1))) ? 'active' : '' ?>">
                    <a class="page-link"
                       href="<?php echo $this->configs['app']['site_url'] ?>/task/index?page=<?php echo($i + 1); ?>"><?php echo($i + 1); ?>
                        <span class="sr-only">(current)</span></a></li>
                <?php
            } ?>
            <li class="page-item">
                <a class="page-link"
                   href="<?php echo $this->configs['app']['site_url'] ?>/task/index?page=<?php echo ($page + 1 <= $pages) ? ($page + 1) : $pages; ?>">Next</a>
            </li>
        </ul>
    </nav>
</div>

