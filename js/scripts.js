$(document).ready(function () {
    $('.post-module').hover(function () {
        $(this).find('.description').stop().animate({
            height: "toggle",
            opacity: "toggle"
        }, 300);
    });

    $(document).on('click','#close-preview',function(){
        $('#modalPreview').modal('hide');
    });

    $(document).on('click','#preview', function(){
        var date = new Date();
        $('#preview-task-image').attr('src',window.location.origin+'/images/avma.jpg');
        $('#preview-task-status').text('new');
        $('#preview-task-title').text($('#task-name').val());
        $('#preview-task-author').text($('#task-author').val());
        $('#preview-task-description').text($('#task-description').text());
        $('#preview-task-created').text(getCurrentDate());
        $('#preview-task-assigned').text($('#user-name').val());
        $('#modalPreview').modal('show');
    });

    function getCurrentDate(){
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth()+1; //January is 0!

        var yyyy = today.getFullYear();
        if(dd<10){
            dd='0'+dd;
        }
        if(mm<10){
            mm='0'+mm;
        }
        var h = today.getHours();
        if(h<10)
            h ='0'+h;
        var m = today.getMinutes();
        if(m<10)
            m ='0'+m;
        var s = today.getSeconds();
        if(s<10)
            s ='0'+s;

        return yyyy+'-'+mm+'-'+dd+' '+h+':'+m+':'+s;


    }

});